﻿using System;
using System.Threading.Tasks;

namespace Tests.Android
{
    public class AsyncTask
    {
        public async void BeginRead(EventHandler ev)
        {
            await Task.Delay(2000);
            
            ev.Invoke(null, null);
        }
    }
}