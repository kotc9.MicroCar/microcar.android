﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using Xunit;

namespace Tests.Android
{
    public class AndroidTests
    {
        [Fact]
        public void Test1()
        {
            Assert.True(true);
        }

        [Fact]
        public async Task AsyncCallback()
        {
            var task = new AsyncTask();

            var x = 0;

            task.BeginRead((sender, args) => { x = 1; });

            await Task.Delay(5000);
            
            Assert.True(x == 1);
        }


    }
}