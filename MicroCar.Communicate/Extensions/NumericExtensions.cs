﻿using System.Text;
using System.Text.RegularExpressions;

namespace MicroCar.Communicate.Extensions
{
    public static class NumericExtensions
    {
        
        public static string String(this byte[] val)
        {
            return Encoding.ASCII.GetString(val);
        }
        
        public static bool IsInt(this string str)
        {
            return 
                !string.IsNullOrEmpty(str) 
                && Regex.IsMatch(str, @"\d");
        }
    }
}