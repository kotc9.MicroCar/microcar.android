﻿using System.Collections.Generic;
using MicroCar.Communicate.Models;

namespace MicroCar.Communicate.Providers
{
    public interface ICommunicateProvider
    {
        bool Write(string data);
        bool Read(out string data);
        bool Connect();
        void Close();
        List<DeviceItem> Devices { get; }
        DeviceItem Device { get; set; }
    }
}