﻿namespace MicroCar.Communicate.Models
{
    public class ReadedEventArgs
    {
        public string RawPackage { get; }

        public ReadedEventArgs(string rawPackage)
        {
            RawPackage = rawPackage;
        }
    }
}