﻿namespace MicroCar.Communicate.Models
{
    public class WritedEventArgs
    {
        public string Writed { get; }

        public WritedEventArgs(string writed)
        {
            Writed = writed;
        }
    }
}