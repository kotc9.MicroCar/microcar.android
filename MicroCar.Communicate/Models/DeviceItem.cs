﻿namespace MicroCar.Communicate.Models
{
    public class DeviceItem
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
