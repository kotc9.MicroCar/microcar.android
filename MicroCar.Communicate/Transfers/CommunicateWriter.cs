﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.Util;
using MicroCar.Communicate.Models;
using MicroCar.Communicate.Providers;

namespace MicroCar.Communicate.Transfers
{
    public class CommunicateWriter
    {
        public event EventHandler<FailedEventArgs> Failed;

        public event EventHandler<WritedEventArgs> Writed;

        private readonly List<string> _dataToSend;
        private readonly int _delayTime;
        private readonly ICommunicateProvider _communicateProvider;
        private bool _isStarted;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="delayTime"> delay time in ms</param>
        /// <param name="communicateProvider"></param>
        /// <param name="dataToSend"></param>
        public CommunicateWriter(int delayTime, ICommunicateProvider communicateProvider,
            List<string> dataToSend = null)
        {
            Failed += (sender, args) => { Log.Info("writer", $"{args.Message}"); };

            if (dataToSend == null)
            {
                dataToSend = new List<string> {"0", "0", "0", "0", "0"};
            }

            _dataToSend = dataToSend;
            _delayTime = delayTime;
            _communicateProvider = communicateProvider;
        }

        public void ChangeData(int order, string data)
        {
            _dataToSend[order] = data;
        }

        public void EndWrite()
        {
            _isStarted = false;
        }

        public async void BeginWriteAsync()
        {
            _isStarted = true;
            while (_isStarted)
            {
                await Task.Delay(_delayTime);
                try
                {
                    var package = StringToPackage();
                    if (!_communicateProvider.Write(package))
                    {
                        Failed?.Invoke(this, new FailedEventArgs("can't write"));
                        break;
                    }

                    Writed?.Invoke(this, new WritedEventArgs(package));
                }
                catch
                {
                    Failed?.Invoke(this, new FailedEventArgs("write failed"));
                    break;
                }
            }
        }

        private string StringToPackage()
        {
            var data = new string[_dataToSend.Count];
            _dataToSend.CopyTo(data);

            var result = data.Aggregate("$", (current, d) => current + $"{d} ");

            result = result.Remove(result.Length - 1);
            result += ";";

            return result;
        }
    }
}