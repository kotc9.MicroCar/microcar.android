﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Android.Util;
using MicroCar.Communicate.Models;
using MicroCar.Communicate.Providers;

namespace MicroCar.Communicate.Transfers
{
    public class CommunicateReader
    {
        public event EventHandler<ReadedEventArgs> Received;
        public event EventHandler<FailedEventArgs> Failed;

        private readonly ICommunicateProvider _provider;
        private bool _isStarted;

        public CommunicateReader(ICommunicateProvider communicateProvider)
        {
            Failed += (sender, args) => { Log.Info("receiver", $"{args.Message}"); };

            _provider = communicateProvider;
        }

        public void EndRead()
        {
            _isStarted = false;
        }

        public async void BeginReadAsync()
        {
            await Task.Run(BeginRead);
        }

        private void BeginRead()
        {
            _isStarted = true;
            while (_isStarted)
            {
                try
                {
                    var (success, rawPackage) = ReadRawPackage();

                    if (success)
                    {
                        Received?.Invoke(this, new ReadedEventArgs(rawPackage));
                    }
                    else
                    {
                        Failed?.Invoke(this, new FailedEventArgs("can't read"));
                    }
                }
                catch
                {
                    Failed?.Invoke(this, new FailedEventArgs("receiver failed"));
                    break;
                }
            }
        }

        private (bool, string) ReadRawPackage()
        {
            var rawPackage = "";

            var element = "p";
            while (element != "\n")
            {
                if (!_provider.Read(out element))
                {
                    return (false, null);
                }

                if (element != "\n")
                {
                    rawPackage += element;
                }
            }

            return (true, rawPackage);
        }
    }
}