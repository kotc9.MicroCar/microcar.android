﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using IO.Github.Controlwear.Virtual.Joystick.Android;
using MicroCar.Android.Extensions;
using MicroCar.Android.Scripts.Communicate;
using MicroCar.Android.Scripts.Enums;
using MicroCar.Android.Scripts.Models;
using MicroCar.Communicate.Models;
using MicroCar.Communicate.Transfers;

namespace MicroCar.Android
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppThemeControl")]
    public class ControlActivity : AppCompatActivity
    {
        private TextView _textViewBattery;
        private TextView _textViewRssi;
        private CommunicateMiddleware _middleware;

        private bool _isPushed;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_control);

            InitializeComponents();
        }

        private void InitializeComponents()
        {
            InitializeLayoutComponents();

            InitializeCommunication();
            
            new ActionTimer(15000, () => { _isPushed = false;}).Start();
        }

        private void InitializeCommunication()
        {
            _middleware = new CommunicateMiddleware(
                new CommunicateWriter(50, Global.Provider),
                new CommunicateReader(Global.Provider));


            if (!_middleware.IsStarted)
            {
                _middleware.Begin();
            }

            _middleware.Failed += OnFailed;
            _middleware.Received += OnReceived;
        }

        private void OnReceived(object sender, ReceivedEventArgs e)
        {
            var battery = e.Package.Battery;
            var rssi = e.Package.Rssi;

            
            
            RunOnUiThread(() =>
            {
                var toPush = "";
                if (battery <= (byte) BatteryOuterValue.Critical && !_isPushed)
                {
                    toPush += $"Низкий заряд аккумулятора ({battery}%)\n";
                }
                if (rssi <= (byte) RssiOuterValue.Critical && !_isPushed)
                {
                    toPush += $"Низкий уровень сигнала ({rssi}%)";
                }

                if (toPush != "")
                {
                    CreateNotification("Alarm", toPush);
                    _isPushed = true;
                }
                
                UpdateStatsText(battery,rssi);
                UpdateStatsTextColor(battery, rssi);
            });
        }

        private void UpdateStatsTextColor(in byte battery, in byte rssi)
        {

            var greenBattery = battery.MapByte(0, 100, 0, 255);
            var redBattery = 255 - greenBattery;

            var greenRssi = rssi.MapByte(0, 100, 0, 255);
            var redRssi = 255 - greenRssi;
            _textViewBattery.SetTextColor(new Color(redBattery, greenBattery, 0));
            _textViewRssi.SetTextColor(new Color(redRssi, greenRssi, 0));
        }

        private void UpdateStatsText(in byte battery, in byte rssi)
        {
            _textViewBattery.Text = $"Battery: {battery}%";
            _textViewRssi.Text = $"Rssi: {rssi}%";
        }

        private void CreateNotification(string header, string title)
        {
            var toast = Toast.MakeText(this, $"{header}\n{title}", ToastLength.Long);
            toast.SetGravity(GravityFlags.Center, 0, 0);
            
            toast.Show();
            
            var v = (Vibrator) GetSystemService(VibratorService);

            v.Vibrate(VibrationEffect.CreateOneShot(500, VibrationEffect.DefaultAmplitude));
        }

        private void OnFailed(object sender, FailedEventArgs e)
        {
            _middleware.End();
            Global.Provider.Close();
            Finish();
        }


        private void InitializeLayoutComponents()
        {
            _textViewBattery = FindViewById<TextView>(Resource.Id.textViewBattery);
            _textViewRssi = FindViewById<TextView>(Resource.Id.textViewRssi);

            var joystickControl1 = FindViewById<JoystickView>(Resource.Id.joystick1Control);
            joystickControl1.Move += JoystickControl1OnMove;

            var joystickControl2 = FindViewById<JoystickView>(Resource.Id.joystick2Control);
            joystickControl2.Move += JoystickControl2OnMove;

            var switchLight = FindViewById<Switch>(Resource.Id.switchControlLight);
            switchLight.CheckedChange += SwitchLightOnCheckedChange;
        }

        private void JoystickControl1OnMove(object sender, JoystickView.MoveEventArgs e)
        {
            var point = e.GetPoint();

            _middleware.ChangeWriteData(CommunicateOrder.Joystick1X, $"{point.X}");
            _middleware.ChangeWriteData(CommunicateOrder.Joystick1Y, $"{point.Y}");
        }

        private void JoystickControl2OnMove(object sender, JoystickView.MoveEventArgs e)
        {
            var point = e.GetPoint();

            _middleware.ChangeWriteData(CommunicateOrder.Joystick2X, $"{point.X}");
            _middleware.ChangeWriteData(CommunicateOrder.Joystick2Y, $"{point.Y}");
        }

        private void SwitchLightOnCheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            var lightState =
                e.IsChecked
                    ? "1"
                    : "0";
            _middleware.ChangeWriteData(CommunicateOrder.LightState, lightState);
        }

        protected override void OnDestroy()
        {
            _middleware.End();
            
            base.OnDestroy();
        }
    }
}