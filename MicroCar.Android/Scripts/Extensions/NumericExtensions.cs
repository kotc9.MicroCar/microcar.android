﻿using System.Text;
using System.Text.RegularExpressions;
using Java.Lang;

namespace MicroCar.Android.Extensions
{
    public static class NumericExtensions
    {
        public static double ToRadians(this int val)
        {
            return Math.Pi / 180 * val;
        }

    }
}