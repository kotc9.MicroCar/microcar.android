﻿using System;
using Android.Graphics;
using IO.Github.Controlwear.Virtual.Joystick.Android;

namespace MicroCar.Android.Extensions
{
    public static class JoystickExtensions
    {
        public static Point GetPoint(this JoystickView.MoveEventArgs e)
        {
            var angle = e.P0;
            var power = e.P1;

            var radians = angle.ToRadians();
            var x = Math.Cos(radians);
            var y = Math.Sin(radians);

            var length = Math.Sqrt(x * x + y * y);
            x /= length;
            y /= length;

            x *= power;
            y *= power;

            return new Point((int) x, (int) y);
        }
    }
}