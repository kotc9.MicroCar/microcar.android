﻿namespace MicroCar.Android.Extensions
{
    public static class MathExtensions
    {
        public static byte MapByte(this byte x, byte in_min, byte in_max, byte out_min, byte out_max)
        {
            return (byte) ((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
        }
    }
}