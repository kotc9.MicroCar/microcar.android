﻿using MicroCar.Android.Scripts.Enums;

namespace MicroCar.Android.Scripts.Models
{
    public class ReceivedPackage
    {
        public byte Battery;
        public byte Rssi;
    }
    
}