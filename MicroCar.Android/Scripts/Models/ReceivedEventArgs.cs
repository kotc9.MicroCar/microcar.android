﻿namespace MicroCar.Android.Scripts.Models
{
    public class ReceivedEventArgs
    {
        public ReceivedPackage Package { get; }

        public ReceivedEventArgs(ReceivedPackage package)
        {
            Package = package;
        }
    }
}