﻿using System;
using System.Linq;
using Android.Util;
using Java.Nio.Channels;
using MicroCar.Android.Extensions;
using MicroCar.Android.Scripts.Enums;
using MicroCar.Android.Scripts.Models;
using MicroCar.Communicate.Extensions;
using MicroCar.Communicate.Models;
using MicroCar.Communicate.Transfers;
using FailedEventArgs = MicroCar.Communicate.Models.FailedEventArgs;
using WritedEventArgs = MicroCar.Communicate.Models.WritedEventArgs;

namespace MicroCar.Android.Scripts.Communicate
{
    public class CommunicateMiddleware
    {
        public event EventHandler<ReceivedEventArgs> Received;
        public event EventHandler<FailedEventArgs> Failed;
        public event EventHandler<WritedEventArgs> Writed;

        private readonly CommunicateReader _reader;
        private readonly CommunicateWriter _writer;

        private bool _failed;

        public bool IsStarted { get; private set; }

        public CommunicateMiddleware(CommunicateWriter writer, CommunicateReader reader)
        {
            _writer = writer;
            _reader = reader;

            _writer.Failed += OnFailed;
            _reader.Failed += OnFailed;

            _reader.Received += OnReceive;
            _writer.Writed += (sender, args) => { Writed?.Invoke(sender, args); };

            Received += (sender, args) =>
            {
                Log.Info("receiver",
                    $"succeful received, battery: {args.Package.Battery}, rssi: {args.Package.Rssi}");
            };

            Writed += (sender, args) => { Log.Info("writer", $"succeful writed {args.Writed}"); };
        }

        public void ChangeWriteData(CommunicateOrder order, string data)
        {
            _writer.ChangeData((int)order, data);
        }

        private void OnReceive(object sender, ReadedEventArgs e)
        {
            var package = PackageFrom(e.RawPackage);

            if (package != null)
            {
                Received?.Invoke(sender, new ReceivedEventArgs(package));
            }
        }

        private void OnFailed(object sender, FailedEventArgs e)
        {
            if (_failed) return;

            _failed = true;
            Failed?.Invoke(this, new FailedEventArgs("middleware failed"));
        }

        public void Begin()
        {
            _writer.BeginWriteAsync();
            _reader.BeginReadAsync();
            IsStarted = true;
        }

        public void End()
        {
            _writer.EndWrite();
            _reader.EndRead();
            IsStarted = false;
        }

        private ReceivedPackage PackageFrom(string raw)
        {
            if (raw[0] != '$' || raw.Length <= 4)
            {
                return null;
            }

            //удаление $ и ;\r
            raw = raw.Substring(1, raw.Length - 3);
            var rawArray = raw.Split();

            if (rawArray.Length != 2 || !IsInt(rawArray[0], rawArray[1]))
            {
                return null;
            }

            var battery = Convert.ToByte(rawArray[0]);
            var rssi = Convert.ToByte(rawArray[1]);

            if (battery > (byte) BatteryOuterValue.Maximum || rssi > (byte) RssiOuterValue.Maximum)
            {
                return null;
            }

            return new ReceivedPackage
            {
                Battery = battery,
                Rssi = rssi
            };

            static bool IsInt(params string[] necessaryStrings)
            {
                return necessaryStrings.All(s => s.IsInt());
            }
        }
    }
}