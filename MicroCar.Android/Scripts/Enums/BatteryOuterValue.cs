﻿namespace MicroCar.Android.Scripts.Enums
{
    public enum BatteryOuterValue : byte
    {
        Minimal = 0,
        Critical = 20,
        Maximum = 100
    }
}