﻿namespace MicroCar.Android.Scripts.Enums
{
    public enum CommunicateOrder : int
    {
        Joystick1X = 0,
        Joystick1Y = 1,
        Joystick2X = 2,
        Joystick2Y = 3,
        LightState = 4
    }
}