﻿using System.Runtime.Remoting.Contexts;
using Android.App;
using Android.Content;
using Android.Gestures;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace MicroCar.Android.Services
{
    [Service]
    public class OverlayService : Service
    {
        public override void OnCreate()
        {
            base.OnCreate();

            Toast.MakeText(this,"I am service", ToastLength.Long);
            var windowManager = (IWindowManager) GetSystemService(WindowService);
            var inflater = (LayoutInflater) GetSystemService(LayoutInflaterService);

            var overlayedButton = new Button(this);
            overlayedButton.Text ="Overlay button";
            overlayedButton.SetBackgroundColor(Color.Aqua);
            
            var overlayView = inflater.Inflate(Resource.Layout.activity_control, null);

            var layoutParams = new WindowManagerLayoutParams(
                ViewGroup.LayoutParams.WrapContent,
                ViewGroup.LayoutParams.WrapContent,
                WindowManagerTypes.Phone,
                WindowManagerFlags.NotFocusable,
                Format.Translucent) {Gravity = GravityFlags.Left | GravityFlags.Top, X = 0, Y = 100};

            windowManager.AddView(overlayedButton, layoutParams);
        }

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }
    }

}