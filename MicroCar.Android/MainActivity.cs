﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Util;
using Android.Widget;
using MicroCar.Communicate.Providers;


namespace MicroCar.Android
{
    [Activity(Label = "@string/app_name", MainLauncher = true, Theme = "@style/AppThemeMain")]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            Global.Provider = new BluetoothProvider();

            InitializeComponents();
        }

        private void InitializeComponents()
        {
            InitializeDropdown();

            var buttonConnect = FindViewById<FloatingActionButton>(Resource.Id.buttonConnect);
            buttonConnect.Click += ButtonConnectOnClick;

            var buttonRefresh = FindViewById<Button>(Resource.Id.buttonRefresh);
            buttonRefresh.Click += ButtonRefreshOnClick;
        }

        private void ButtonRefreshOnClick(object sender, EventArgs e)
        {
            InitializeDropdown();
        }

        private void ButtonConnectOnClick(object sender, EventArgs e)
        {
            if (Global.Provider.Device == null)
            {
                Log.Info("ButtonConnect", "device is null");
                return;
            }

            StartActivity(typeof(ConnectActivity));
        }

        private void InitializeDropdown()
        {
            var dropdown = FindViewById<Spinner>(Resource.Id.dropdownCommunicator);
            var communicatorDevices = Global.Provider.Devices;

            if (communicatorDevices == null) return;

            var names = communicatorDevices.Select(item => item.Name).ToArray();
            var adapter = new ArrayAdapter<string>(this, Resource.Layout.support_simple_spinner_dropdown_item,
                names);
            dropdown.Adapter = adapter;

            dropdown.ItemSelected += DropdownOnItemSelected;
        }

        private void DropdownOnItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var spinner = (Spinner) sender;

            var item = (string) spinner.GetItemAtPosition(e.Position);

            Global.Provider.Device = Global.Provider.Devices.Find(d => d.Name == item);
        }
    }
}